/*
  Copyright (C) 2002  Apu�Paquola - Instituto de Qu�mica -
                    Universidade de S�o Paulo - Brasil

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include <zerg.h>

static int
not_here(char *s)
{
    croak("%s not implemented on this architecture", s);
    return -1;
}

static double
constant_PE(char *name, int len, int arg)
{
    if (2 + 6 >= len ) {
	errno = EINVAL;
	return 0;
    }
    switch (name[2 + 6]) {
    case 'I':
	if (strEQ(name + 2, "RCENT_IDENTITIES")) {	/* PE removed */
#ifdef PERCENT_IDENTITIES
	    return PERCENT_IDENTITIES;
#else
	    goto not_there;
#endif
	}
    case 'P':
	if (strEQ(name + 2, "RCENT_POSITIVES")) {	/* PE removed */
#ifdef PERCENT_POSITIVES
	    return PERCENT_POSITIVES;
#else
	    goto not_there;
#endif
	}
    }
    errno = EINVAL;
    return 0;

not_there:
    errno = ENOENT;
    return 0;
}

static double
constant_P(char *name, int len, int arg)
{
    switch (name[1 + 0]) {
    case 'E':
	return constant_PE(name, len, arg);
    case 'O':
	if (strEQ(name + 1, "OSITIVES")) {	/* P removed */
#ifdef POSITIVES
	    return POSITIVES;
#else
	    goto not_there;
#endif
	}
    }
    errno = EINVAL;
    return 0;

not_there:
    errno = ENOENT;
    return 0;
}

static double
constant_Q(char *name, int len, int arg)
{
    if (1 + 5 >= len ) {
	errno = EINVAL;
	return 0;
    }
    switch (name[1 + 5]) {
    case 'A':
	if (strEQ(name + 1, "UERY_ANNOTATION")) {	/* Q removed */
#ifdef QUERY_ANNOTATION
	    return QUERY_ANNOTATION;
#else
	    goto not_there;
#endif
	}
    case 'E':
	if (strEQ(name + 1, "UERY_END")) {	/* Q removed */
#ifdef QUERY_END
	    return QUERY_END;
#else
	    goto not_there;
#endif
	}
    case 'F':
	if (strEQ(name + 1, "UERY_FRAME")) {	/* Q removed */
#ifdef QUERY_FRAME
	    return QUERY_FRAME;
#else
	    goto not_there;
#endif
	}
    case 'L':
	if (strEQ(name + 1, "UERY_LENGTH")) {	/* Q removed */
#ifdef QUERY_LENGTH
	    return QUERY_LENGTH;
#else
	    goto not_there;
#endif
	}
    case 'N':
	if (strEQ(name + 1, "UERY_NAME")) {	/* Q removed */
#ifdef QUERY_NAME
	    return QUERY_NAME;
#else
	    goto not_there;
#endif
	}
    case 'O':
	if (strEQ(name + 1, "UERY_ORIENTATION")) {	/* Q removed */
#ifdef QUERY_ORIENTATION
	    return QUERY_ORIENTATION;
#else
	    goto not_there;
#endif
	}
    case 'S':
	if (strEQ(name + 1, "UERY_START")) {	/* Q removed */
#ifdef QUERY_START
	    return QUERY_START;
#else
	    goto not_there;
#endif
	}
    }
    errno = EINVAL;
    return 0;

not_there:
    errno = ENOENT;
    return 0;
}

static double
constant_SC(char *name, int len, int arg)
{
    if (2 + 3 > len ) {
	errno = EINVAL;
	return 0;
    }
    switch (name[2 + 3]) {
    case '\0':
	if (strEQ(name + 2, "ORE")) {	/* SC removed */
#ifdef SCORE
	    return SCORE;
#else
	    goto not_there;
#endif
	}
    case '_':
	if (strEQ(name + 2, "ORE_BITS")) {	/* SC removed */
#ifdef SCORE_BITS
	    return SCORE_BITS;
#else
	    goto not_there;
#endif
	}
    }
    errno = EINVAL;
    return 0;

not_there:
    errno = ENOENT;
    return 0;
}

static double
constant_SU(char *name, int len, int arg)
{
    if (2 + 6 >= len ) {
	errno = EINVAL;
	return 0;
    }
    switch (name[2 + 6]) {
    case 'A':
	if (strEQ(name + 2, "BJECT_ANNOTATION")) {	/* SU removed */
#ifdef SUBJECT_ANNOTATION
	    return SUBJECT_ANNOTATION;
#else
	    goto not_there;
#endif
	}
    case 'E':
	if (strEQ(name + 2, "BJECT_END")) {	/* SU removed */
#ifdef SUBJECT_END
	    return SUBJECT_END;
#else
	    goto not_there;
#endif
	}
    case 'F':
	if (strEQ(name + 2, "BJECT_FRAME")) {	/* SU removed */
#ifdef SUBJECT_FRAME
	    return SUBJECT_FRAME;
#else
	    goto not_there;
#endif
	}
    case 'L':
	if (strEQ(name + 2, "BJECT_LENGTH")) {	/* SU removed */
#ifdef SUBJECT_LENGTH
	    return SUBJECT_LENGTH;
#else
	    goto not_there;
#endif
	}
    case 'N':
	if (strEQ(name + 2, "BJECT_NAME")) {	/* SU removed */
#ifdef SUBJECT_NAME
	    return SUBJECT_NAME;
#else
	    goto not_there;
#endif
	}
    case 'O':
	if (strEQ(name + 2, "BJECT_ORIENTATION")) {	/* SU removed */
#ifdef SUBJECT_ORIENTATION
	    return SUBJECT_ORIENTATION;
#else
	    goto not_there;
#endif
	}
    case 'S':
	if (strEQ(name + 2, "BJECT_START")) {	/* SU removed */
#ifdef SUBJECT_START
	    return SUBJECT_START;
#else
	    goto not_there;
#endif
	}
    }
    errno = EINVAL;
    return 0;

not_there:
    errno = ENOENT;
    return 0;
}

static double
constant_S(char *name, int len, int arg)
{
    switch (name[1 + 0]) {
    case 'C':
	return constant_SC(name, len, arg);
    case 'U':
	return constant_SU(name, len, arg);
    }
    errno = EINVAL;
    return 0;

not_there:
    errno = ENOENT;
    return 0;
}

static double
constant_D(char *name, int len, int arg)
{
    if (1 + 11 >= len ) {
	errno = EINVAL;
	return 0;
    }
    switch (name[1 + 11]) {
    case 'A':
	if (strEQ(name + 1, "ESCRIPTION_ANNOTATION")) {	/* D removed */
#ifdef DESCRIPTION_ANNOTATION
	    return DESCRIPTION_ANNOTATION;
#else
	    goto not_there;
#endif
	}
    case 'E':
	if (strEQ(name + 1, "ESCRIPTION_EVALUE")) {	/* D removed */
#ifdef DESCRIPTION_EVALUE
	    return DESCRIPTION_EVALUE;
#else
	    goto not_there;
#endif
	}
    case 'H':
	if (strEQ(name + 1, "ESCRIPTION_HITNAME")) {	/* D removed */
#ifdef DESCRIPTION_HITNAME
	    return DESCRIPTION_HITNAME;
#else
	    goto not_there;
#endif
	}
    case 'S':
	if (strEQ(name + 1, "ESCRIPTION_SCORE")) {	/* D removed */
#ifdef DESCRIPTION_SCORE
	    return DESCRIPTION_SCORE;
#else
	    goto not_there;
#endif
	}
    }
    errno = EINVAL;
    return 0;

not_there:
    errno = ENOENT;
    return 0;
}

static double
constant_E(char *name, int len, int arg)
{
    switch (name[1 + 0]) {
    case 'N':
	if (strEQ(name + 1, "ND_OF_REPORT")) {	/* E removed */
#ifdef END_OF_REPORT
	    return END_OF_REPORT;
#else
	    goto not_there;
#endif
	}
    case 'V':
	if (strEQ(name + 1, "VALUE")) {	/* E removed */
#ifdef EVALUE
	    return EVALUE;
#else
	    goto not_there;
#endif
	}
    }
    errno = EINVAL;
    return 0;

not_there:
    errno = ENOENT;
    return 0;
}

static double
constant(char *name, int len, int arg)
{
    errno = 0;
    switch (name[0 + 0]) {
    case 'A':
	if (strEQ(name + 0, "ALIGNMENT_LENGTH")) {	/*  removed */
#ifdef ALIGNMENT_LENGTH
	    return ALIGNMENT_LENGTH;
#else
	    goto not_there;
#endif
	}
    case 'B':
	if (strEQ(name + 0, "BLAST_VERSION")) {	/*  removed */
#ifdef BLAST_VERSION
	    return BLAST_VERSION;
#else
	    goto not_there;
#endif
	}
    case 'D':
	return constant_D(name, len, arg);
    case 'E':
	return constant_E(name, len, arg);
    case 'G':
	if (strEQ(name + 0, "GAPS")) {	/*  removed */
#ifdef GAPS
	    return GAPS;
#else
	    goto not_there;
#endif
	}
    case 'I':
	if (strEQ(name + 0, "IDENTITIES")) {	/*  removed */
#ifdef IDENTITIES
	    return IDENTITIES;
#else
	    goto not_there;
#endif
	}
    case 'N':
	if (strEQ(name + 0, "NOHITS")) {	/*  removed */
#ifdef NOHITS
	    return NOHITS;
#else
	    goto not_there;
#endif
	}
    case 'P':
	return constant_P(name, len, arg);
    case 'Q':
	return constant_Q(name, len, arg);
    case 'S':
	return constant_S(name, len, arg);
    case 'U':
	if (strEQ(name + 0, "UNMATCHED")) {	/*  removed */
#ifdef UNMATCHED
	    return UNMATCHED;
#else
	    goto not_there;
#endif
	}
    }
    errno = EINVAL;
    return 0;

not_there:
    errno = ENOENT;
    return 0;
}


MODULE = Zerg		PACKAGE = Zerg		


double
constant(sv,arg)
    PROTOTYPE: DISABLE
    PREINIT:
	STRLEN		len;
    INPUT:
	SV *		sv
	char *		s = SvPV(sv, len);
	int		arg
    CODE:
	RETVAL = constant(s,len,arg);
    OUTPUT:
	RETVAL



void
zerg_open_file(filename)
	char* filename
    CODE:
	zerg_open_file(filename);

void
zerg_close_file()
    CODE:
	zerg_close_file();

void
zerg_ignore(code)
	int code
    CODE:
	zerg_ignore(code);

void
zerg_ignore_all()
    CODE:
	zerg_ignore_all();


void
zerg_unignore(code)
	int code
    CODE:
	zerg_unignore(code);


void
zerg_unignore_all()
    CODE:
	zerg_unignore_all();

void
zerg_get_token()
    PPCODE:
	int c;
	char *v;
	zerg_get_token(&c, &v);
	XPUSHs(sv_2mortal(newSViv(c)));
	XPUSHs(sv_2mortal(newSVpv(v,0)));


int
zerg_get_token_offset()
    CODE:
	RETVAL=zerg_get_token_offset();
    OUTPUT:
	RETVAL
